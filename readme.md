## Whisper

Whisper is a RESTful API server written using Express/Node. I wrote this project to teach myself:
- server-side programming
- NoSQL databases
- html templating
- cloud hosting

Started this project in ASP.Net Core, regretted the move few hours later. It's incredibly hard to write C# code in macOS. Visual Studio Code has come a long way, but it's still far far behind proper IDEs like Xcode. The development workflow felt very clunky. Plus I don't particularly like C# syntax, it's approaching the verbosity of Objective-C that I duly hate. Too.much.namespacing.kills.innocent.puppies.

Next on the list was Swift. Apple open sourced Swift late last year and people have been churning out Swift frameworks like crazy ever since. The best I could find was PerfectlySoft's Perfect. It's a HTTP framework written for server-side Swift. Took me a whole day to publish "hello, world". The toolchain doesn't work with Xcode at the moment, Apple has **finally** opened up Xcode *just a little* for third-party plugins and extensions so hopefully things will get better. But I'm no fan of writing servers in plain text editors and command line, so it was goodbye to Swift.

I wasn't going to touch Java. #NeverForget

So settled in on Node.js. The absence of strong typing wasn't as bad as I had expected except a few 'undefined' every now and then. All in all enjoyed working with Node. If I get to do this again, will definitely write it all in Typescript. JavaScript code is very hard to read, type identifiers bring in much-needed context.  

The project makes use of:
- Node.js
- Express.js 
- MongoDB for database
- Jade for HTML templating
- Heroku for hosting

#### Demo
The server supports the following routes:

- `GET` /api/v1/users
- `GET` /api/v1/users/id
- `POST` /api/v1/users
- `PUT` /api/v1/users/id
- `DELETE` /api/v1/users/id

The error screens are all a bit of CSS and JADE magic.

#### Next steps
Sleep. 

#### License
Feel free to use all or parts in your own projects, personal or business. No attributions, please. 

#### Feedback
Have some tips for me or just want to say hi? ping me at hello@animesh.ltd with "Whisper-Node" in the subject line.