//
// server.js
// Whisper-Node
// 
// Created on 26/08/2016 by Animesh.
// Copyright (c) 2016. All rights reserved.
// 

var logger = require("morgan")
var express = require("express")
var bodyParser = require("body-parser")
var jade = require("jade")
var api = require("./api")
var database = require("./database")
var user = require("./routes/user")

var app = express()
var hostname = api.hostname
var port = api.port

app.use(bodyParser.json())

// Enable logging
app.use(logger("dev"))

// Homepage
var html = jade.compileFile("./views/index.jade")
app.get("/", function(request, response) { 
    response.status(200).send(html({
        name: "Whisper",
        description: "A barebones RESTful API server written using Express/Node.js, Jade and MongoDB.",
        hostname: api.hostname,
        version: api.version
    }))
})


// Lock out non-public versions
app.all("/api/:version/*", (request, response, next) => {
    if(api.doesSupport(request.params.version)) {
        next()
    }
    else {
        response.status(403).send(api.errors.deprecated(request.url))
    }
})

// User routes
var userRoute = "/api/:version/users"
app.get(`${userRoute}/:name`, user.find)
app.post(userRoute, user.create)
app.put(`${userRoute}/:id`, user.update)
app.delete(`${userRoute}/:id`, user.delete)


// Catch-all for unavailable routes
app.all("*", (request, response) => {
    response.status(418).send(api.errors.catchall())
})

// Connect to database and start listening
database.init(function (error) {
    if(error) {
        throw error
    }
    app.listen(port, function() {
    console.log(`Server listening at http://${hostname}:${port}`)
    })
})
