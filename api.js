//
// api.js
// Whisper-Node
// 
// Created on 26/08/2016 by Animesh.
// Copyright (c) 2016. All rights reserved.
// 

var jade = require("jade")
var html = jade.compileFile("././views/error.jade")

var api = {}

api.version     = "v1"
api.hostname    = process.env.HOSTNAME || "localhost"
api.port        = process.env.PORT || 8181

api.doesSupport = function (requestedVersion) {
    if(requestedVersion == this.version) {
        return true
    }
    return false 
}

//
// API homepage
//
api.serveHomePage = function(request, response) {
    return html({
        name: "Whisper",
        description: "A data sync API for Whisper clients",
        hostname: api.hostname,
        version: api.version
    })
} 

//
// Error handling
// 

api.errors = {}

api.errors.deprecated = function(requestPath) {
    var message = "This version of the API is not available publicly. Please try " + api.version + " instead."
    console.log(message)

    var path = api.hostname + requestPath

    return html({
        message: message,
        hint: path
    })
}

api.errors.notFound = function(resource) {
    var message = "Couldn't find " + resource + "."
    return html({
        message: message,
        hint: "HTTP 404"
    })
}

api.errors.requestFailed = function(error) {
    return html({
        message: "I've failed you 😞",
        hint: error
    })
}

api.errors.catchall = function() {
    return html({
        message: "My maker didn't design me to do such ghastly things.",
        hint: "HTTP 418"
    })
}

module.exports = api

