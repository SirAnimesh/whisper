//
// user.js
// Whisper-Node
// 
// Created on 26/08/2016 by Animesh.
// Copyright (c) 2016. All rights reserved.
// 

var parser = require("body-parser")
var express = require("express")
var objectID = require("bson-objectid")
var db = require("../database")
var api = require("../api")

var user = {}

// GET /users/:name
user.find = function(request, response) {
    var username = request.params.name
    console.log("Retrieving " + username)
    db.users.findOne({"username": username}, function(error, user) {
        if(error) {
            throw error
        }
        if(user != null) {
            response.status(200).send(user)
        }
        else {
            response.status(404).send(api.errors.notFound("users/" + username))
        }
    })
}

// POST /users
user.create = function(request, response) {
    var user = request.body
    console.log("Adding user: " + JSON.stringify(user))
    db.users.insert(user, {safe: true}, function(error, result) {
        if(error) {
            response.status(500).send(api.errors.requestFailed)
        }
        else {
            response.status(200).send("User added successfully.")
        }
    })
}

// PUT /users/:id
user.update = function(request, response) {
    var id = request.params.id
    var newUser = request.body
    db.users.update({"_id": objectID(id)}, newUser, {safe: true}, function(error, result) {
         if(error) {
            response.status(500).send(api.errors.requestFailed)
        }
        else {
            response.status(200).send("User updated successfully.")
        }
    })
}

// DELETE /users/:id
user.delete = function(request, response) {
    var id = request.params.id
    db.users.remove({"_id": objectID(id)}, {safe: true}, function(error, result) {
        if(error) {
            response.status(500).send(api.errors.requestFailed)
        }
        else {
            response.status(200).send("User removed successfully.")
        }
    })
}

module.exports = user